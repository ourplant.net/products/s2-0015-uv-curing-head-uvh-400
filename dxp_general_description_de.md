Der UV Curing Head ist mit einem UV-LED-Spot zum Aushärten und Voraktivieren von photoaktiven Klebstoffen ausgestattet. Dieser Arbeitskopf ist mit verschiedenen Linsen erhältlich.

| **Linse** | **max. Intensität** **(mW/cm²)** | **Arbeitsabstand (mm)** | **Spotdurchmesser (mm)** |
| --------- | -------------------------------- | ----------------------- | ------------------------ |
| A         | 12000                            | 3                       | 0,6                      |
| B         | 5000                             | 6                       | 1                        |
| C         | 2000                             | 8                       | 2                        |
| D         | 1100                             | 10                      | 3                        |

Die Intensität der Linse ist per Software individuell einstellbar.
Die Linse B ist im Lieferumfang enthalten. Die Linsen A,C, und D sind optional bestellbar.
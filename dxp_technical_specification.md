The S2-0015 UV-CURING HEAD UVH-400 has the following technical specifications.

## Technical information
| Dimensions in mm (W x D x H) | 49 x 224 x 514 |
| :--------------------------- | -------------- |
| Weight in kg                 | 4              |
| Movement range in Z in mm    | 150            |
| Voltage in V                 | 24             |
| Max. current in A            | 4              |
| Communication interface      | UNICAN         |
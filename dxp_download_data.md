Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0015-uv-curing-head-uvh-400).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/S2-0015-uv-curing-head-uvh-400/-/raw/main/01_operating_manual/S2-0015_A1_Betriebsanleitung.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/S2-0015-uv-curing-head-uvh-400/-/raw/main/09_assembly_drawing/s2-0015_A_ZNB_uv-curing_head_uvh-400.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/S2-0015-uv-curing-head-uvh-400/-/raw/main/02_Schaltplan/S2-0015-EPLAN-A.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/S2-0015-uv-curing-head-uvh-400/-/raw/main/04_spare_parts/S2-0015-EVL-B.pdf), [en](https://gitlab.com/ourplant.net/products/S2-0015-uv-curing-head-uvh-400/-/raw/main/04_spare_parts/S2-0019_B_EVL%20engl.pdf)

<!-- 2021 (C) Häcker Automation GmbH -->
